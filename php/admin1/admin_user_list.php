<?php

session_start();
$_SESSION["success_msg"]='';
if(!isset($_SESSION['email_user'])){
header("location: ../../forms/admin_form.php");
}

include '../../php/database.php';

$table_name= 'user';
$column_name1='type';
$column_value1='user';
$all_column='*';
//pagination 
if (isset($_GET['pageno'])) {
$pageno = $_GET['pageno'];
} else {
$pageno = 1;
}
$limit = 5;
$offset = ($pageno-1) * $limit;
$conn = $db->return_conn();
$total_pages_sql = "SELECT COUNT(*) FROM $table_name where $column_name1='$column_value1'";
$result = mysqli_query($conn,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $limit);


if(isset($_POST['submit']) && !empty($_POST['search_box']) && $_POST['status'] == 'all') {
    $value = trim($_POST['search_box']);
    $query = "SELECT $all_column FROM $table_name where name LIKE '%$value%' OR mob_no LIKE '%$value%' OR email LIKE '%$value%' LIMIT $offset,$limit";
    $result = mysqli_query($conn,$query); 


}elseif (isset($_POST['submit']) && !empty($_POST['search_box'])) {
    $status = $_POST['status'];
    $value = trim($_POST['search_box']);
    $query = "SELECT $all_column FROM $table_name where name LIKE '%$value%' OR mob_no LIKE '%$value%' OR email LIKE '%$value%' && status='$status' LIMIT $offset,$limit";
    $result = mysqli_query($conn,$query);
} 


else {
    //record according to offset and limit
    $query = "SELECT $all_column FROM $table_name where $column_name1='$column_value1' LIMIT $offset,$limit";
    $result = mysqli_query($conn,$query);   
    
}








?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Accountability</title>
    <!-- <link rel="stylesheet" href="../../css/welcome.css"> -->

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->
	<script src="../../js/crude.js"></script>
	<!-- <script src="../../js/user_filter.js"></script> -->

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <style>
        footer {
            height: 80px;
        }
       
    </style>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin_welcome.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Admin</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="admin_welcome.php">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <!-- <div class="sidebar-heading">
        Custom User
      </div> -->

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item active">
       
        <a class="nav-link" href="../admin/admin_user_list.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Users</span></a>
      
        <!-- <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Users:</h6>
                <a class="collapse-item" href="../php/admin/admin_delete_form.php">Delete</a>
                <a class="collapse-item" href="../php/admin/admin_edit_form.php">Edit</a>
            </div>
        </div> -->
    </li>

            <!-- Divider -->
    <hr class="sidebar-divider">

    
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
            </a>
            <!-- Dropdown - Messages -->
            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small"
                            placeholder="Search for..." aria-label="Search"
                            aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </li>

    <!-- Nav Item - Alerts -->
    <li class="nav-item dropdown no-arrow mx-1">
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
            aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">
                Alerts Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="mr-3">
                    <div class="icon-circle bg-primary">
                        <i class="fas fa-file-alt text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="mr-3">
                    <div class="icon-circle bg-success">
                        <i class="fas fa-donate text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="mr-3">
                    <div class="icon-circle bg-warning">
                        <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
        </div>
    </li>

        <!-- Nav Item - Messages -->
        <!-- <li class="nav-item dropdown no-arrow mx-1">
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                    Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="dropdown-list-image mr-3">
                        <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60"
                            alt="">
                        <div class="status-indicator bg-success"></div>
                    </div>
                    <div class="font-weight-bold">
                        <div class="text-truncate">Hi there! I am wondering if you can help me with a
                            problem I've been having.</div>
                        <div class="small text-gray-500">Emily Fowler · 58m</div>
                    </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="dropdown-list-image mr-3">
                        <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60"
                            alt="">
                        <div class="status-indicator"></div>
                    </div>
                    <div>
                        <div class="text-truncate">I have the photos that you ordered last month, how
                            would you like them sent to you?</div>
                        <div class="small text-gray-500">Jae Chun · 1d</div>
                    </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="dropdown-list-image mr-3">
                        <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60"
                            alt="">
                        <div class="status-indicator bg-warning"></div>
                    </div>
                    <div>
                        <div class="text-truncate">Last month's report looks great, I am very happy with
                            the progress so far, keep up the good work!</div>
                        <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                    </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="dropdown-list-image mr-3">
                        <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60"
                            alt="">
                        <div class="status-indicator bg-success"></div>
                    </div>
                    <div>
                        <div class="text-truncate">Am I a good boy? The reason I ask is because someone
                            told me that people say this to all dogs, even if they aren't good...</div>
                        <div class="small text-gray-500">Chicken the Dog · 2w</div>
                    </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
            </div>
        </li> -->

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><i><?php echo $_SESSION['email_user']; ?></i></b></span>
                <img class="img-profile rounded-circle"
                    src="">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profile
                </a>
                
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>

    </ul>

</nav>
<!-- End of Topbar -->

                <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> </a> -->
        </div>
        <div>
        <div>
            <form action="admin_user_list.php" method="post" name="search_form" >
            <div class="row">    
            <div class="col-lg-4">
            Search: <input type="text" name="search_box" id='search_box' value="<?php echo $value; ?>" >
            <button type="submit" class="btn-default" name='submit' id='submit'>search</button>

            </div>
        
            <div class="col-lg-1">
                <select name="status" id="status">
                <option value="all">all</option>
                <option value="active">active</option>
                <option value="inactive">inactive</option>
                </select>
            </div>
        </div>
        </form>
        </div>
        <hr>

      

<p id="message" style="display:block;color:green;"></p>


<div>
    <table class="table table-striped table-condensed table-bordered">
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>mobile no</th>
            <th>Email</th>
            <th>status</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <?php

$i = 0;
while($row = mysqli_fetch_assoc($result)){ $i++;
?>
        <tr>
            <td><?php echo $row["id"]?> </td>
            <td><?php echo $row["name"] ?> </td>
            <td> <?php echo $row["mob_no"] ?></td>
            <td class ="<?php echo "email_".$i; ?>" data-value="<?php echo $row["email"]; ?>"> <?php echo $row["email"] ?> </td>
            <td>
            <?php 
                if($row["status"] == 'inactive') { ?>
                        <a href="#" class ="activate"  data-value="<?php echo $row["id"]; ?>">
                   <b><i style="color:green" id="cell"><?php echo $row["status"] ?></i></b></a>
                <?php } else { ?>
                        <span class="text-primary"><?php echo $row["status"] ?></span>
                <?php } ?>
            </td>
            <td><a href="#" class ="edit" data-value="<?php echo $row["id"]; ?>" ><b><i>Edit</i></b></a>
            </td>
            <td>
            <?php
            
                if($row["status"] == 'active'){ ?>
            <a href="#" class ="delete" style=  "color:red" data-value="<?php echo $row["id"]; ?>"><b><i>delete</i></b></a>
                <?php } else { ?>
                <span class="text-primary"><?php echo $row["status"] ?></span>
            <?php } ?>
        
            </td>
        </tr>
        <?php
}
?>

    </table>
</div>
<div>
    <ul class="pagination pagination-lg">
        <li class="page-item <?php if($pageno <= 1){ echo 'disabled'; } else{echo 'active';} ?>">
            <a class="page-link"
                href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">&laquo;</a>  
        </li>
            <?php
                if ($total_pages <= 10){
                    for ($counter = 1; $counter <= $total_pages; $counter++){
                        if ($counter == $pageno) {?>
                            <li class='page-item active'>
                            <a class="page-link"
                             href="?pageno=1"> <?php echo $counter ?></a>
                             </li><?php
                        }else{?>
                             <li class="page-item"><a class="page-link" href='?pageno=<?php echo $counter; ?>'> <?php echo $counter; ?></a></li>
                        <?php }


                    }
                }

?>
            







        <li class="page-item <?php if($pageno >= $total_pages){ echo 'disabled'; } else{echo "active";} ?>">
            <a class="page-link"
                href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">&raquo;</a>
        </li>
    </ul>
</div>




    </div>
    <!-- /.container-fluid -->

                <!-- </div> -->
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer fixed-bottom bg-white height=100">
                    <div class="container my-auto">
                        <div class="footer-copyright text-center py-3"><b>Contact:9867505780</b>
                            <a href="mailto:atulkumar.upadhay05@gmail.com"
                                class='contentemail'><b>atulkumar.upadhyay05@gmail.com</b></a>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

                <!-- </div> -->
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <!-- Scroll to Top Button-->
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fas fa-angle-up"></i>
            </a>

            <!-- Logout Modal-->
            <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Select "Logout" below if you are ready to end your current session.
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-primary" href="admin_logout.php">Logout</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- inactive  Modal-->
            <div class="modal fade" id="admin_active_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Active user?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Select "Activate" below if you want to activate user.
                        <div><input class="id_active" name="id" type="hidden"></div>
                        
                        </div>
                        <div class="modal-footer">
                       
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-primary" id="submit_active" type="submit" href="admin_user_list.php">Activate</a>
                        </div>
                    </div>
                </div>
            </div>

             <!-- delete  Modal-->
             <div class="modal fade" id="admin_delete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Delete user?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Select "Delete" below if you want to Delete user.
                        <div><input class="id_delete" name="id" type="hidden"></div>
                        
                        </div>
                        <div class="modal-footer">
                       
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-primary" id="submit_delete" type="submit" href="admin_user_list.php">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- edit modal -->
            <div class="modal fade" id="admin_edit_modal" data-value="<?php echo $row["id"]; ?>"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit user?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Select "Update" below if you want to Edit user.
                       
                        
                        <form action="#" method="post" id="myForm">
                            <style>
                            .modal_error {color : red;}
                            </style>
                           
                            <div class="form-group">
                                <label for="name" id="name_title">Name:</label>
                                <span class="modal_error" id="name_error">*</span>
                                <input type="name" class="form-control" id="name" placeholder="Enter name" name="user[name]"
                                    value="">

                            </div>
                            <div class="form-group">
                                <label for="mob_no" id="mob_no_title">Mobile number:</label>
                                <span  class="modal_error" id='mob_no_error'>*</span>				
                                <input type="text" class="form-control" id="mob_no" onkeypress="return restrictAlphabets(event)"
                                    maxlength="10" value="" placeholder="Enter mobile no"
                                    name="user[mob_no]">
                            </div>
                            <div class="form-group">
                                
                                <label for="email" id="email_title">email:</label><span class='abc' id='abc'></span>
                                <span  class="modal_error" id='email_error'>*</span>
                                <input type="name" class="form-control" id="email" placeholder="Enter email" name="user[email]"
                                    value="">
                            </div>
                            <div><input class="id_edit" name="id" type="hidden"></div>

                        </form>
                        
                        </div>
                        <div class="modal-footer">
                       
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-primary" id="submit_edit" type="submit" name="submit_edit" href="admin_user_list.php">Update</a>
                        </div>
                    </div>
                </div>
            </div>
        
            <!-- Bootstrap core JavaScript-->
            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

            <!-- Core plugin JavaScript-->
            <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

            <!-- Custom scripts for all pages-->
            <script src="js/sb-admin-2.min.js"></script>

            <!-- Page level plugins -->
            <!-- <script src="vendor/chart.js/Chart.min.js"></script> -->

            <!-- Page level custom scripts -->
            <!-- <script src="js/demo/chart-area-demo.js"></script> -->
            <!-- <script src="js/demo/chart-pie-demo.js"></script> -->

</body>

</html>