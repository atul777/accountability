    <!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="admin_logout.php">Logout</a>
            </div>
        </div>
    </div>
</div>

            <!-- inactive  Modal-->
<div class="modal fade" id="admin_active_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Active user?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Select "Activate" below if you want to activate user.
                <div><input class="id_active" name="id" type="hidden"></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="submit_active" type="submit" href="admin_user_list.php">Activate</a>
            </div>
        </div>
    </div>
</div>
<!-- delete  Modal-->
<div class="modal fade" id="admin_delete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete user?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Select "Delete" below if you want to Delete user.
                <div><input class="id_delete" name="id" type="hidden"></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="submit_delete" type="submit" href="admin_user_list.php">Delete</a>
            </div>
        </div>
    </div>
</div>
<!-- edit modal -->
<div class="modal fade" id="admin_edit_modal" data-value="<?php echo $row["id"]; ?>"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit user?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Select "Update" below if you want to Edit user.
                <form action="#" method="post" id="myForm">
                    <style>
                        .modal_error {color : red;}
                    </style>
                    <div class="form-group">
                        <label for="name" id="name_title">Name:</label>
                        <span class="modal_error" id="name_error">*</span>
                        <input type="name" class="form-control" id="name" placeholder="Enter name" name="user[name]"
                            value="">
                    </div>
                    <div class="form-group">
                        <label for="mob_no" id="mob_no_title">Mobile number:</label>
                        <span  class="modal_error" id='mob_no_error'>*</span>               
                        <input type="text" class="form-control" id="mob_no" onkeypress="return restrictAlphabets(event)"
                            maxlength="10" value="" placeholder="Enter mobile no"
                            name="user[mob_no]">
                    </div>
                    <div class="form-group">
                        <label for="email" id="email_title">email:</label><span class='abc' id='abc'></span>
                        <span  class="modal_error" id='email_error'>*</span>
                        <input type="name" class="form-control" id="email" placeholder="Enter email" name="user[email]"
                            value="">
                    </div>
                    <div><input class="id_edit" name="id" type="hidden"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="submit_edit" type="submit" name="submit_edit" href="admin_user_list.php">Update</a>
            </div>
        </div>
    </div>
</div>

<!-- add user -->

<div class="modal fade" id="addUserModal" data-value="<?php ?>"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add user?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Select "Add" below if you want to Add user.
                 <form action="signupModal.php" id="addmyForm" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name" id="addname_title">Name:</label>
                        <span class="modal_error">*<?php echo $error['empty_name']; echo $error['nameErr']; ?></span>
                        <input type="text" class='form-control' placeholder="Enter Name" name="user[name]" id="addname"
                            value="<?php echo $error['name']; echo $error_email['name'] ;?>">
                    </div>

                    <div class="form-group">
                        <label for="mob_no" id="addmob_no_title"><b>Mobile number</b></label>
                        <span class="modal_error">*<?php echo $error['empty_mob_no']; echo $error['mob_noErr']; ?> </span>
                        <input type="text" class="form-control" placeholder="Enter Mobile number" maxlength="10"
                            onkeypress="return restrictAlphabets(event)" inputmode="number" name="user[mob_no]" id="addmob_no"
                            value="<?php echo $error['mob_no']; echo $error_email['mob_no']; ?>">
                    </div>

                    <div class="form-group">
                        <label for="email" id="addemail_title"><b>Email id</b></label>
                        <span
                            class="modal_error">*<?php echo $error['empty_email']; echo $error['emailErr']; echo $error_email['email_exists']; ?></span>
                        <input type="text" class="form-control" placeholder="Enter Email id"
                            value="<?php echo $error['email']; echo $error_email['email']; ?>" name="user[email]" id="addemail">
                    </div>

                    <div class="form-group">
                        <label for="password" id="addpassword_title"><b>Password</b></label>
                        <span class="modal_error">*<?php echo $error['empty_password']; echo $error['passwordErr'];?> </span>
                        <input type="password" class="form-control" placeholder="Password" name="user[password]"
                            value="<?php echo $error['password']; echo $error_email['password']; ?>" id="addpassword">
                        <input type="checkbox"
                            onchange="document.getElementById('password').type = this.checked ? 'text' : 'password'"> Show
                        password</input>
                    </div>
                    <div class="form-group">
                        <label for="image">Upload Your Image</label>
                        <input type="file" class="form-control-file" id="addimage" name="user[image]" accept=".png, .jpg, .jpeg">
                    </div>
                    <div class='row'>
                        <!-- <div class='col-lg-6'>
                            <a href="../forms/user_form.php"><button type="button" class="btn btn-primary btn-block"
                                    id="redirect">login</button>
                            </a></div> -->
                        <div class='col-lg-3'>
                            <button type="submit" class="btn btn-primary btn-block" id="addsubmit" name="submit">Add</button>
                        </div>
                    </div>
                    <hr>
                    <div class='error'>
                        <?php echo $error['invalid'] ?>
                    </div>

                </form>
              
            </div>
            <!-- <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="submit" type="submit" name="submit" href="admin_user_list.php">Add</a>
            </div> -->
        </div>
    </div>
</div>