<?php
include '../../php/database.php';
session_start();
// $_SESSION['msg'] = NULL;
if(!isset($_SESSION['email_user'])){
header("location: ../forms/admin_form.php");
}
  
// activate user
if(isset($_POST['id']) && $_POST['type'] == 'active_user'){
    $id = $_POST['id'];
    $table_name='user';
    // echo "hello";
    $column_name1='status';
    $column_name2='id';
    $column_value1='active';
    $db->delete_user($table_name,$column_name1,$column_name2,$column_value1,$id);    
}
    


// delete user
if(isset($_POST['id']) && $_POST['type'] == 'delete_user'){
$id = $_REQUEST['id'];
$table_name='user';
$column_name1='status';
$column_name2='id';
$column_value1='inactive';
$db->delete_user($table_name,$column_name1,$column_name2,$column_value1,$id);
$_SESSION['msg'] == 'user deleted successfully';
echo $_SESSION['msg'];
}


//get user data
if(isset($_POST['id']) && $_POST['type'] == 'get_userdata'){
    $id = $_REQUEST['id'];
    $table_name='user';
    $column_name = 'id';
    $result= $db->user_data($table_name,$column_name,$id);
    echo $result['name'].'|'.$result['mob_no'].'|'.$result['email'];
}


//edit user
if(isset($_POST['id']) && $_POST['type'] == 'user_edit'){
    $id = $_REQUEST['id'];
    $table_name ='user';
    $column_name1 ='name';
    $column_name2 ='mob_no';
    $column_name3 ='email';
    $column_name4 ='id';
    
    $db->update_table($table_name,$column_name1,$column_name2,$column_name3,$column_name4,$_REQUEST['name'],$_REQUEST['mob_no'],$_REQUEST['email'], $id);
}
if(isset($_POST['id']) && $_POST['type'] == 'email_exists'){
    $id = $_REQUEST['id'];
    $email = $_REQUEST['user_email'];
    $table_name = 'user';
    $column_name1 = 'email';
    $column_name2 = 'id';
    $column_value2 = $id;
    $result = $db->num_rows($table_name,$column_name1,$column_name2,$email,$column_value2);
    echo $result;
}


?>