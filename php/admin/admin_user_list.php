<?php

session_start();
$_SESSION["success_msg"]='';
if(!isset($_SESSION['email_user'])){
header("location: ../../forms/admin_form.php");
}

include '../../php/database.php';
include 'pagination_logic.php';
$conn = $db->return_conn();

// echo "<pre>";
// print_r($_POST);
// echo "</pre>";

$type_column='type';
$user_type='user';
$column_name1='type';
$column_value1='user';
$all_column='*';
//pagination 
if (isset($_GET['pageno'])) {
$pageno = $_GET['pageno'];
} else {
$pageno = 1;
}
$limit = 5;
$offset = ($pageno-1) * $limit;
// echo $offset;
{
	// $conn = $db->return_conn();
	// $total_pages_sql = "SELECT COUNT(*) FROM $table_name where $column_name1='$column_value1'";
	// $result = mysqli_query($conn,$total_pages_sql);
	// $total_rows = mysqli_fetch_array($result)[0];
	// $total_pages = ceil($total_rows / $limit);
}



if(isset($_POST['submit']) && !empty($_POST['search_box']) && $_POST['status'] == 'all') {
    $value = trim($_POST['search_box']);
    $result = $pg->for_all($all_column,$offset,$value,$limit,$conn);
	$total_pages = $pg -> total_pages_for_default($type_column,$user_type,$limit,$conn);

}elseif (isset($_POST['submit']) && !empty($_POST['search_box'])) {
    $status = $_POST['status'];
    $value = trim($_POST['search_box']);
    $result = $pg->for_status($all_column,$offset,$value,$status,$limit,$conn);
	$total_pages = $pg -> total_pages_for_status($offset,$value,$status,$limit,$conn);
	// echo $total_pages;

} 
elseif (isset($_POST['submit']) && $_POST['status'] != 'all') {
    $status = $_POST['status'];
    // $value = 'ive';
    $result = $pg->for_only_status($all_column,$offset,$status,$limit,$conn);
	// $total_pages = $pg -> total_pages_for_default($offset,$value,$status,$limit,$conn);
} 

else {
    //record according to offset and limit
    $result = $pg->for_default($all_column,$column_name1,$column_value1,$offset,$limit,$conn);
	$total_pages = $pg -> total_pages_for_default($type_column,$user_type,$limit,$conn);
	// echo $total_pages;
}








?>


<?php include('partial/header.php');
?>

<?php include('partial/sidebar.php');
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> User List
 </h1>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="span">	<?php if (isset($_GET['success'])) {
    		echo $_GET['success'];
    	} ?>
    </div>
			<div>
				<!-- search box -->
			    <form action="admin_user_list.php" method="post" name="search_form" >
			        <div class="row">
			            <div class="col-lg-3">
			                Search: <input type="text" name="search_box" id='search_box' value="<?php echo $value; ?>" >
	                 	</div>
	                	<div class="col-lg-1">
			                <select name="status" id="status">
			                    <option value="all"><?php if (isset($_POST['submit'])) {
			                    	echo $_POST['status'];
			                    }else { echo 'all'; }; ?></option>
			                    <option value="active">active</option>
			                    <option value="inactive">inactive</option>
			                    <option value="all">all</option>
			                </select>
		             	</div>
		             	<div class="col-lg-1">
			                <button type="submit" class="btn-primary" name='submit' id='submit'>search</button>
		            	</div>
		            	<div class="col-lg-2">
		            		<button type="button" class="btn-primary" id="addUser">
          						<span class="glyphicon glyphicon-plus"></span> Add user
        					</button>
		            	</div>
		            </div>
			    </form>       
	        </div>
			    
		
			<hr>
			<p id="message" style="display:block;color:green;"></p>
			<!-- table			 -->
			<div>
			    <table class="table table-striped table-condensed table-bordered">
			        <tr>
			            <th>id</th>
			            <th>Name</th>
			            <th>mobile no</th>
			            <th>Email</th>
			            <th>images</th>
			            <th>status</th>
			            <th>Edit</th>
			            <th>Delete</th>
			        </tr>
			        <?php
			            $i = 0;
			            while($row = mysqli_fetch_assoc($result)){ $i++;
			            ?>
			        <tr>
			            <td><?php echo $row["id"]?> </td>
			            <td><?php echo $row["name"] ?> </td>
			            <td> <?php echo $row["mob_no"] ?></td>
			            <td class ="<?php echo "email_".$i; ?>" data-value="<?php echo $row["email"]; ?>"> <?php echo $row["email"] ?> </td>
			            <td><?php echo '<a href = "../../images/'.$row["id"].'/'.$row["image"].'" target="new"><img height= "20px" width="20px" src = "../../images/'.$row["id"].'/'.$row["image"].'"></a>'; ?></td>

			            <td>
			                <?php 
			                    if($row["status"] == 'inactive') { ?>
			                <a href="#" class ="activate" id="activate" onclick="activateUser(<?php echo $row["id"]; ?>); return false;">
			                <b><i style="color:green" id="cell"><?php echo $row["status"] ?></i></b></a>
			                <?php } else { ?>
			                <span class="text-primary"><?php echo $row["status"] ?></span>
			                <?php } ?>
			            </td>
			            <td><a href="#" class ="edit" onclick="editUser(<?php echo $row["id"]; ?>); return false;" ><b><i>Edit</i></b></a>
			            </td>
			            <td>
			                <?php
			                    if($row["status"] == 'active'){ ?>
			                <a href="#" class ="delete" style=  "color:red" onclick="DeleteUser(<?php echo $row["id"]; ?>); return false;"><b><i>delete</i></b></a>
			                <?php } else { ?>
			                <span class="text-primary"><?php echo $row["status"] ?></span>
			                <?php } ?>
			            </td>
			        </tr>
			        <?php
			            }
			            ?>
			    </table>
			</div>
			<!-- pagination	 -->
			<?php include 'pagination.php'; ?>
			<!-- pagination ends -->


    </section>
  </div>




<?php include 'admin_modal.php'; ?>

<?php include('partial/footer.php'); ?>
