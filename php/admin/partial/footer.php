<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2019 /</strong> 
     <a href="mailto:atulkumar.upadhay05@gmail.com"
                                class='contentemail'><b>atulkumar.upadhyay05@gmail.com</b></a>
                                <b>/ Contact:9867505780</b>
</footer>

  </div>
<!-- ./wrapper -->
<!-- jquery minified -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jquery file for user list -->
<script src="../../../js/crude.js"></script>

<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->

<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->

<script src="dist/js/adminlte.min.js"></script>

<script src="../../js/addUserModal.js"></script>
<!-- <link rel="stylesheet" href="../../css/signup_form.css"> -->
<!-- Sparkline -->

<!-- <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script> -->
<!-- jvectormap  -->

<!-- SlimScroll -->

<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->

<!-- <script src="bower_components/chart.js/Chart.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- <script src="dist/js/pages/dashboard2.js"></script> -->
<!-- AdminLTE for demo purposes -->

<!-- <script src="dist/js/demo.js"></script> -->
</body>
</html>
