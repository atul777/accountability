<div>
    <ul class="pagination pagination-lg">
        <li class="page-item <?php if($pageno <= 1){ echo 'disabled'; } else{echo 'active';} ?>">
            <a class="page-link"
                href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">&laquo;</a>  
        </li>
        <?php
            if ($total_pages <= 15){
                for ($counter = 1; $counter <= $total_pages; $counter++){
                    if ($counter == $pageno) {?>
        <li class='page-item active'>
            <a class="page-link"
                href="?pageno=1"> <?php echo $counter ?></a>
        </li>
        <?php
            }else{?>
        <li class="page-item"><a class="page-link" href='?pageno=<?php echo $counter; ?>'> <?php echo $counter; ?></a></li>
        <?php }
            }
            }
            
            ?>
        <li class="page-item <?php if($pageno >= $total_pages){ echo 'disabled'; } else{echo "active";} ?>">
            <a class="page-link"
                href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">&raquo;</a>
        </li>
    </ul>
</div>