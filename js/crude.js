 $(document).ready(function() {
    
    // activate user   
    // $("#activate").click(function() {
    //     var activate = $(this).attr('data-value');
    //     $(".id_active").val(activate);
    //     $("#admin_active_modal").modal();      
    //     active();
    // })

   // delete user 

    // $(".delete").click(function() {
    //     var delete_user = $(this).attr('data-value');
    //     $(".id_delete").val(delete_user);
    //     $("#admin_delete_modal").modal();      
    //     delete_user_fn();
    // })
   // edit user 
   // $(".edit").click(function() {
   //      var edit_user = $(this).attr('data-value');
   //      $(".id_edit").val(edit_user);
   //      $("#admin_edit_modal").modal();      
   //      get_userdata_fn();
   //      edit_validation_fn();
   //      // ajax_email();
   //  })

   //add new user
   $("#addUser").click(function() {
        $("#addUserModal").modal();      

    })
    
    // ajax on blur
    $("#email").blur(function () {
        $(".modal_error").remove();
        var email = $("#email").val();
        if (email.length > 1) {
            var regex_email = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var result_email = regex_email.test(email);
            if (!result_email) {
                $('#email_title').after(`<span class="modal_error">Enter valid email(must contain '@' and '.')</span>`);

                return false;
            }
            else{
                ajax_email();
            }
            
        }   
    });
    
})
//end of document ready   

   
//jquery for modal    
function edit_validation_fn (){
    $('#submit_edit').click(function() {
        $(".modal_error").remove();
        var name = $("#name").val();
        var mob_no = $("#mob_no").val();
        var email = $("#email").val();
        var error = [];

        //name validation
        if (name.length < 1) {
            error['empty_name'] = "This field is required";
            $('#name_title').after(`<span class="modal_error">${error['empty_name']}</span>`);            
            return false;
        } else if (name.length > 1) {
            var regex_name = /^[a-zA-Z ]{2,30}$/;
            var result_name = regex_name.test(name);
            if (!result_name) {
                error['error_name'] = "invalid input(alphabets only)";
                $('#name_title').after(`<span class="modal_error">${error['error_name']}</span>`);
                return false;
            }
        }
        //mob_no validation
        if (mob_no.length < 1) {
            error['empty_mob_no'] = "This field is required";
            $('#mob_no_title').after(`<span class="modal_error">${error["empty_mob_no"]}</span>`);
            return false;
        } else if (mob_no.length < 10 || mob_no.length > 10) {
            error['error_empty']="Invalid input(10 numbers)";
            $('#mob_no_title').after(`<span class="modal_error">${error['error_empty']}</span>`);
            return false;
        }
        //email validation
        if (email.length < 1) {
            error['empty_email']="This field is required";
            $('#email_title').after(`<span class='modal_error'>${error['empty_email']}</span>`);    
            return false;
        }    
        else if (email.length > 1) {
            var regex_email = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var result_email = regex_email.test(email);
            if (!result_email) {
                error['error_email']="Enter valid email(must contain '@' and '.')";
                $('#email_title').after(`<span class="modal_error">${error['error_email']}</span>`);
                return false;
            }
            else{
                ajax_email();
            } 
            
        }
        var span = $('.modal_error').html();
        if(span == 'Already exists') {
            return false
        }
        else{
            user_edit_fn();   
        }

    })
}   
   
   
   
   
// function active
function active (){
    $("#submit_active").click(function() {
        var id = $(".id_active").val();
        //ajax code    
        jQuery.ajax({
            type: "POST",
            // async : false,
            url: "../../php/admin/admin_ajax.php",
            data: {
                id: id,
                type : 'active_user'
            },
            success: function (response) {
                var tmp = response;
                $("#admin_active_modal").modal('hide');      
                
            }
        });  
    })
}

//ajax for email
function ajax_email(){
    $(".modal_error").remove();
    var email = $("#email").val();
    var user_id = $(".id_edit").val();

    //ajax code
    jQuery.ajax({
        type: "POST",
        url: "../../php/admin/admin_ajax.php",
        async: false,
        data: {
            user_email: email,
            id: user_id,
            type : 'email_exists'
        },
        success: function (response) {
            if (response == 0) {
                $('#email_title').after(`<span class="modal_error">OK</span>`);
            } else if (response != 0) {                            
                $('#email_title').after(`<span class="modal_error">Already exists</span>`);
            }
        }

    });    
}


//function delete
function delete_user_fn(){
    $("#submit_delete").click(function() {
    
        var id = $(".id_delete").val();
        //ajax code    
        jQuery.ajax({
            type: "POST",
            // async : false,
            url: "../../php/admin/admin_ajax.php",
            data: {
                id: id,
                type : 'delete_user'
            },
            success: function (response) {
            var tmp = response;
            // $("#message").html('user deleted successfully');
            $("#admin_delete_modal").modal('hide');  
            
            }
        });  
    })
}

//get user data click on edit click
function get_userdata_fn(){
        var id = $(".id_edit").val();
        //ajax code    
        jQuery.ajax({
            type: "POST",
            async : false,
            url: "../../php/admin/admin_ajax.php",
            data: {
                id: id,
                type : 'get_userdata'
            },  
            success: function (response) {
            var tmp = response;
            var user_data = tmp.split("|");
            $("#name").val(user_data[0]);
            $("#mob_no").val(user_data[1]);
            $("#email").val(user_data[2]);
        }
    });    
}



//user edit on edit_submit click
function user_edit_fn(){
    var id = $(".id_edit").val();
    var name = $("#name").val();
    var mob_no = $("#mob_no").val();
    var email = $("#email").val();
    //ajax code    
    jQuery.ajax({
        type: "POST",
        async : false,
        url: "../../php/admin/admin_ajax.php",
        data: {
            id: id,
            type : 'user_edit',
            name : name,
            mob_no : mob_no,
            email : email
        },  
            success: function (response) {
            
            var tmp = response;
            
           // $("#admin_edit_modal").modal('hide');      
            
        }
    });    
}

 function activateUser(attrValue) {
        // var activate = $(this).attr('data-value');
        $(".id_active").val(attrValue);
        $("#admin_active_modal").modal();      
        active();
    }
     function editUser(attrValue) {
        // var edit_user = $(this).attr('data-value');
        $(".id_edit").val(attrValue);
        $("#admin_edit_modal").modal();      
        get_userdata_fn();
        edit_validation_fn();

    }
     function deleteUser(attrValue) {
       // var delete_user = $(this).attr('data-value');
        $(".id_delete").val(attrValue);
        $("#admin_delete_modal").modal();      
        delete_user_fn();
    }
 
/*code: 48-57 Numbers
8  - Backspace,
35 - home key, 36 - End key
37-40: Arrow keys, 46 - Delete key*/
function restrictAlphabets(e) {
    var x = e.which || e.keycode;
    if ((x >= 48 && x <= 57) || x == 8 ||
    (x >= 35 && x <= 40) || x == 46)
    return true;
    else
    return false;
}


